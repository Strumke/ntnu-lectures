import gymnasium as gym
import tensorflow as tf
import numpy as np
import sys
import dqn_agent
import wandb


env = gym.make('FrozenLake-v1', map_name="4x4", is_slippery=False)
        #render_mode="human")
print("Environment's action space: ", env.action_space)
                                            # 0: Move left
                                            # 1: Move down
                                            # 2: Move right
                                            # 3: Move up
print("Environment's observation space: ", env.observation_space)
                                            # int in [0,15] =
                                            # current_row * ncols + current_col
                                            # (both row and col start at 0)
def preprocess_state(state):
    map_size = 4
    num_states = map_size ** 2
    onehot_vector = tf.one_hot(state, num_states)
    return np.expand_dims(onehot_vector, axis=0)

agent = dqn_agent.DQNAgent(env)

# start a new wandb run to track this script
wandb.init(
    # set the wandb project where this run will be logged
    project="frozenlake",

    # track hyperparameters and run metadata
    config={
    "epsilon_max": agent.epsilon_max,
    }
)

max_steps = 200
episodes = 3000
reward_history = []
total_steps = 0

for _ep in range(episodes):
    if _ep % 10 == 0: print("Episode ", _ep)
    state, _ = env.reset()
    state = preprocess_state(state)
    done = False
    truncated = False
    episode_reward = 0
    ep_steps = 0

    #for _ in range(1):
    for _step in range(max_steps):
        total_steps += 1
        ep_steps += 1
        #env.render()                        # renders one time step, i.e. a slice

        #action = env.action_space.sample()  # samples from the action space
        action = agent.select_action(state)

        next_state, reward, done, truncated, _ = env.step(action)
                                            # returns a tuple with five entries
                                            # 1) new state, from the action
                                            # 2) reward (1 if reached goal, otherwise 0)
                                            # 3) done (boolean)
                                            # 4) truncated, max steps reached (boolean)
                                            # 5) info dict. Not used for training.
        next_state = preprocess_state(next_state)


        agent.replay_buffer.store(state, action, next_state, reward, done)
        if len(agent.replay_buffer) > agent.batch_size:
            agent.learn()

            if total_steps % agent.target_update_freq == 0:
                agent.target_update()

        state = next_state
        episode_reward += reward

        if done or truncated:
            if episode_reward == 1:
                print(f"Reached goal in episode {_ep}")
            wandb.log({"Episode steps": ep_steps,
                "Reached goal": episode_reward,
                "Epsilon max": agent.epsilon_max})
            break

    reward_history.append(episode_reward)

    agent.update_epsilon()

print(f"Reached goal in {np.array(reward_history).sum()/len(reward_history)*100}% of plays")
agent.save_values()
env.close()
