import gymnasium as gym
import tensorflow as tf
import numpy as np
import sys
import dqn_agent

def preprocess_state(state):
    map_size = 4
    num_states = map_size ** 2
    onehot_vector = tf.one_hot(state, num_states)
    return np.expand_dims(onehot_vector, axis=0)


env = gym.make('FrozenLake-v1', map_name="4x4", is_slippery=False, render_mode="human")
agent = dqn_agent.DQNAgent(env, epsilon_max=0.1)

done, truncated = False, False
state, _ = env.reset()
state = preprocess_state(state)
while not done:
    env.render()
    action = agent.select_action(state)
    print(state)
    print(action)
    state, reward, done, truncated, _ = env.step(action)
    state = preprocess_state(state)
    if done or truncated:
        break


