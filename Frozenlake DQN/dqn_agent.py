import random
import sys
import numpy as np
import os
import tensorflow as tf
from replay_buffer import ExperienceReplayBuffer

class DQNAgent:
    def __init__(self, env, epsilon_max=0.999, epsilon_min=0.01,
            epsilon_decay=0.999, batch_size=32, update_frequency=10,
            learning_rate=0.001, discount=0.9, replay_size=4000):

        self.epsilon_max = epsilon_max
        self.epsilon_min = epsilon_min
        self.epsilon_decay = epsilon_decay
        self.discount = discount

        self.action_space = env.action_space
        self.observation_space = env.observation_space

        self.loss_func = tf.keras.losses.MeanSquaredError()
        self.optimizer = tf.keras.optimizers.SGD(learning_rate=learning_rate)

        self.batch_size = batch_size
        self.replay_buffer_size = replay_size
        self.target_update_freq = update_frequency

        self.replay_buffer = ExperienceReplayBuffer(self.replay_buffer_size)

        # Q-value approximator
        self.modelfile = "model.keras"
        self.Q_network = self.load_model()
        # Stable target
        self.target_network = self.create_model()
        self.target_network.set_weights(self.Q_network.get_weights())

    def create_model(self):
        num_actions = self.action_space.n
        input_dim = self.observation_space.n

        model = tf.keras.models.Sequential([
            tf.keras.Input(shape=(input_dim,)),
            tf.keras.layers.Dense(12, activation='relu'),
            tf.keras.layers.Dense(8, activation='relu'),
            tf.keras.layers.Dense(num_actions, activation='linear')
            ])

        model.compile(optimizer='SGD', loss='mean_squared_error', metrics=['accuracy'])
        model.summary()
        print("Created model from scratch")
        return model

    def preprocess_state(self, state):
        map_size = 4
        num_states = map_size ** 2
        onehot_vector = tf.one_hot(state, num_states)
        return tf.expand_dims(onehot_vector, axis=0)

    def update_epsilon(self):
        self.epsilon_max = max(self.epsilon_min, self.epsilon_max * self.epsilon_decay)

    def select_action(self, state):
        # Epsilon-greedy strategy: with probability epsilon, take a random action
        if np.random.random() < self.epsilon_max:
            return self.action_space.sample()

        Q_values = self.Q_network(state)
        action = tf.argmax(Q_values, axis=1).numpy()[0]
        return action

    def learn(self):
        """ Q-learning: y_train = Q_{t+1} """

        states, actions, next_states, rewards, dones = self.replay_buffer.sample(self.batch_size)
        states = tf.squeeze(states, axis=1)
        next_states = tf.squeeze(next_states, axis=1)

        next_target_q_value = tf.reduce_max(self.target_network(next_states), axis=1)
        # Use tf.where to set next_target_q_value to 0 where dones is True
        # (long version of next_target_q_value[dones] = 0)
        next_target_q_value = tf.where(dones,
                tf.zeros_like(next_target_q_value), next_target_q_value)

        y_train = rewards + (self.discount * next_target_q_value)

        # Wrap the forward pass and loss calculation in a tape block to record operations
        with tf.GradientTape() as tape:

            predicted_q = self.Q_network(states)
            # Gather the predicted Q-values corresponding to the selected actions
            batch_indices = tf.range(self.batch_size, dtype=tf.int64)
            indices = tf.stack([batch_indices, actions], axis=1)
            y_pred = tf.gather_nd(predicted_q, indices)

            loss = self.loss_func(y_train, y_pred)

        gradients = tape.gradient(loss, self.Q_network.trainable_variables)
        self.optimizer.apply_gradients(zip(gradients, self.Q_network.trainable_variables))

    def target_update(self):
        self.target_network.set_weights(self.Q_network.get_weights())
        print("Target network updated")
        return

    def load_model(self):
        if os.path.isfile(self.modelfile):
            model = tf.keras.models.load_model(self.modelfile)
            print(f"Loaded model {self.modelfile}")
        else:
            model = self.create_model()
        return model

    def save_values(self):
        try:
            os.remove(self.modelfile)
        except:
            pass
        self.Q_network.save(self.modelfile)
