import sys, os
import random
import numpy as np
import csv
import pickle

def keywithmaxval(d, minimum=False):
    """ a) create a list of the dict's keys and values;
        b) return the key with the max value


    Based on https://stackoverflow.com/questions/268272/getting-key-with-maximum-value-in-dictionary"""
    k=list(d.keys())
    # Boltzmann
    v = np.array(list(d.values()))
    # If there are multiple max values, choose randomly
    if minimum:
        return k[int(random.choice(np.argwhere(v == np.amin(v))))]
    return k[int(random.choice(np.argwhere(v == np.amax(v))))]

class Player(): # Any player, default human. i.e. keyboard input

    def __init__(self, tag):
        self.tag = tag

    def move(self, state):
        while True:
            try:
                move_i =  input("Choose move index: ")
                if move_i == 'x':
                    print("Run while you can, human.")
                    sys.exit()
                move_i =  int(move_i)
                if move_i > len(state)-1 or move_i < 0:
                    raise ValueError("Move index larger than board")
                elif str(move_i) not in list(state):
                    raise ValueError("Move index taken")
            except ValueError:
                print("Please choose an available move index inside the board")
                continue
            break

        return move_i


class QLearnAgent(Player):
    """ Agent for learning, Q-learning, model-free"""
    def __init__(self, tag, exploration_factor=0.5, alpha=0.5, gamma=0.9):
        super().__init__(tag)
        self.tag = tag
        self.alpha = alpha
        self.exp_factor = exploration_factor
        self.exp_min = 0.01
        self.epsilon_decay=0.995
        self.gamma = gamma
        self.values = dict()

    def update_exp_factor(self):
        if self.exp_factor > self.exp_min:
            self.exp_factor *= self.epsilon_decay

    def choose_action(self, state):

        available_moves = [_i for _i, _s in enumerate(state) if _s.isnumeric()]

        # If the current_state does not exist in the Q table, insert it
        if state not in self.values.keys():
            move_vs_qvalue = dict()
            for move in available_moves:
                move_vs_qvalue[move] = 0

            self.values[state] = move_vs_qvalue

        if random.uniform(0, 1) < self.exp_factor:
            action = random.choice(available_moves)

        else:  # Exploit qtable
            action = keywithmaxval(self.values[state])

        self.action = action

        return action

    def learn(self, state, action, reward, new_state, done):
        v_s = self.values[state][action]
        # Optimal future reward is based on the new state
        try:
            v_s_next = max(self.values[new_state].values())
        except KeyError:  # If the tree for the new state has not been made yet, use 0
            v_s_next = 0

        td_target = reward + (0 if done else self.gamma*v_s_next)
        td_delta = td_target - v_s

        new_qvalue = v_s + self.alpha*td_delta

        self.values[state][action] = new_qvalue

        return

    def load_values(self):
        s = 'values_' + self.tag + '.pkl'
        with open(s, 'rb') as f:
            self.values = pickle.load(f)

    def save_values(self):
        s = 'values_' + self.tag + '.pkl'
        try:
            os.remove(s)
        except:
            pass
        with open(s, 'wb') as f:
            pickle.dump(self.values, f)