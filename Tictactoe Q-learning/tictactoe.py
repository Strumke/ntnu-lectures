import numpy as np
import sys, os
import pickle
import random
from tabulate import tabulate
import matplotlib.pyplot as plt
import csv
import player
from watch_tictactoe import plot_heatmap

class TicTacToe():

    def __init__(self):
        self.state = '012345678'

        self.winner = None
        self.player_turn = 'X'

        self.Xcount = 0
        self.Ocount = 0
        self.Tcount = 0
        self.all_count = 0

    def reset(self):
        self.state = '012345678'
        self.winner = None
        self.player_turn = 'X'
        return self.state, {}

    def step(self, action):
        tag = self.player_turn
        state = self.state
        new_state = state[:action] + tag + state[action+1:]

        self.state = new_state

        win = self.check_winner()
        if win:
            if self.winner == tag:
                return new_state, 10, True, {}
            else:
                return new_state, -10, True, {}

        elif not any(_s.isnumeric() for _s in new_state):
            self.winner = "Tie"
            self.Tcount += 1
            return new_state, 1, True, {}

        self.player_turn = 'X' if tag == 'O' else 'O'

        return new_state, 0, False, {}

    def check_winner(self):
        lines = [[0,1,2],[3,4,5],[6,7,8],
                [0,3,6],[1,4,7],[2,5,8],
                [0,4,8],[2,4,6]]

        s = self.state
        for _l in lines:
            candidate = ''.join((s[_l[0]], s[_l[1]], s[_l[2]]))
            if candidate == "XXX":
                self.winner = 'X'
                self.Xcount += 1
                return True
            elif candidate == "OOO":
                self.winner = 'O'
                self.Ocount += 1
                return True
        return False

    def render(self, render_mode="computer"):
        if render_mode == "human":
            f = list(self.state)

        else:
            s = list(self.state)
            f = ['X' if _s=='X' else 'O' if _s=='O' else ' ' for _s in s]

        print(f"  --------------")
        print(f"    {f[0]} | {f[1]} | {f[2]}")
        print("  --------------")
        print(f"    {f[3]} | {f[4]} | {f[5]}")
        print("  --------------")
        print(f"    {f[6]} | {f[7]} | {f[8]}")
        print("  --------------")
        return

# ----------------------------------------

def train_agent(env, agent1, agent2, episodes, max_steps_per_episode=9):
    rewards_all_episodes1, rewards_all_episodes2 = [], []

    # Loop through all episodes
    for episode in range(episodes):
        state, _ = env.reset()  # Reset the environment and get the initial state
        done = False

        states = [state]
        actions = []
        player1 = True
        for step in range(max_steps_per_episode):
            if player1:
                current_player = agent1
                other_player = agent2
            else:
                current_player = agent2
                other_player = agent1

            # Choose action based on the current state using the Q-learning policy
            action = current_player.choose_action(states[-1])
            actions.append(action)

            # Take the action, observe new state and reward
            new_state, reward, done, _ = game.step(action)
            states.append(new_state)

            # Update Q-table for other agent using the learning rule
            if len(states) > 2:
                other_player.learn(states[-3], actions[-2], -reward, states[-1], done)

            player1 = not player1

            if done:
                current_player.learn(states[-2], action, reward, states[-1], done)
                break

        # Decaying is done every episode if episode number is within decaying range
        agent1.update_exp_factor()
        agent2.update_exp_factor()

        if episode % 100 == 0:
            print("Episode: ", episode)
            
    return rewards_all_episodes1, rewards_all_episodes2

# ----------------------------------------

def play_human(game, player1, player2, plotQ=False):
    done = False
    state = game.state

    while not done:
        tag = game.player_turn
        active_player = player1 if player1.tag == tag else player2

        # Print what's going on for human players
        if type(active_player) == player.Player:
            game.render(render_mode="human")

            action = active_player.move(state)
        else:
            if plotQ:
                print("Board for AI agent to move on:")
                game.render()
                plot_heatmap(active_player, state)
            action = active_player.choose_action(state)

        state, reward, done, _ = game.step(action)

        win = game.check_winner()
        if win:
            print("Winner: ", game.winner)
            game.render()

        if done:
            break

        game.player_turn = 'X' if tag == 'O' else 'O'

def pickle_to_csv(filename):
    with open(filename, 'rb') as f:
        dictionary = pickle.load(f)
    csv_filename = filename[:-3]+"csv"
    try:
        os.remove(csv_filename)
    except OSError:
        pass
    a = csv.writer(open(csv_filename, 'a'))
    for v, k in dictionary.items():
        a.writerow([v, k])

def just_play():
    human_2 = player.Player(tag='X')
    human_1 = player.Player(tag='O')
    game = TicTacToe()
    play_human(game, human_1, human_2)

if __name__ == "__main__":

    # --- Play as two humans
    #just_play()

    game = TicTacToe()

    # --- Train the  monster
    TRAIN = False #True
    if TRAIN:
        n_episodes = int(1e6)
        agent_x = player.QLearnAgent(tag='X', exploration_factor=0.3)
        agent_o = player.QLearnAgent(tag='O', exploration_factor=0.3)

        try:
            agent_x.load_values()
            agent_o.load_values()
            print("Continue training from existing Q values")
        except FileNotFoundError:
            print("Training from scratch")
            pass

        train_agent(game, agent_x, agent_o, n_episodes)

        agent_x.save_values()
        agent_o.save_values()
        pickle_to_csv("values_X.pkl")
        pickle_to_csv("values_O.pkl")

        print(f"Trained for {n_episodes} episodes.")

        agent_x.exp_factor = 0
        agent_o.exp_factor = 0

    # --- Combat the AI
    else:
        agent_x = player.QLearnAgent(tag='X', exploration_factor=0)
        agent_x.load_values()
        print("Loaded Q-table for AI player")
        print(agent_x.values['012345678'])

    N = 3
    human_win, AI_win = 0, 0
    print(f"Let's play {N} games!")

    for i in range(N):
        game.reset()
        print("Let's duel!")

        human_player = player.Player(tag='O')
        play_human(game, agent_x, human_player, plotQ=True)
        # Or opposite
        #human_player = player.Player(tag='X')
        #play_human(game, human_player, agent_o)

        if game.winner == human_player.tag:
            print("\nzomgz, you beat an AI!\n")
            human_win += 1
        elif game.winner.lower() == 'tie':
            print("\nAI is growing tired of your silly games.\n")
        else:
            print("\nAI is getting scary good.\n")
            AI_win += 1
    print(f"Human win: {human_win}")
    print(f"AI win: {AI_win}")
    print(f"AI had split focus due to taking over the world: {N-AI_win-human_win}")
