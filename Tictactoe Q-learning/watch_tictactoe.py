import random
from tabulate import tabulate
import matplotlib.pyplot as plt
import csv
from abc import abstractmethod
import player
import numpy as np
import pandas as pd
import sys

def plot_heatmap(agent, state):
    # Initialize a 3x3 grid
    grid = np.zeros((3, 3))

    # Fill the grid with values from the data_dict
    try:
        data_dict = agent.values[state]
        for key, value in data_dict.items():
            row = key // 3
            col = key % 3
            grid[row, col] = value
    except KeyError:
        return

    # Create the heatmap
    plt.imshow(grid, cmap='Greens', interpolation='nearest')
    plt.colorbar(label='Q-value')

    # Annotate the heatmap with the values
    for (i, j), value in np.ndenumerate(grid):
        plt.text(j, i, f'{value:.2f}', ha='center', va='center', color='black')

    # Set axis labels and title
    plt.xticks(range(3), ['Col 1', 'Col 2', 'Col 3'])
    plt.yticks(range(3), ['Row 1', 'Row 2', 'Row 3'])
    plt.title('State Q-value Heatmap')
    plt.show()

if __name__ == "__main__":
    agent_x = player.QLearnAgent(tag='X', exploration_factor=0.3)
    agent_x.load_values()
    print(agent_x.values['012345678'])
    state0 = '012345678'
    plot_heatmap(agent_x, state0)

#def print_game(state):
#    s = list(state)
#    f = ['X' if _s=='X' else 'O' if _s=='O' else ' ' for _s in s]
#
#    print(f"  --------------")
#    print(f"    {f[0]} | {f[1]} | {f[2]}")
#    print("  --------------")
#    print(f"    {f[3]} | {f[4]} | {f[5]}")
#    print("  --------------")
#    print(f"    {f[6]} | {f[7]} | {f[8]}")
#    print("  --------------")
#    return
#
#def find_next_moves(state, move='O'):
#    next_states = []
#    for i, char in enumerate(state):
#        # Check if the position is empty (i.e., contains a digit)
#        if char.isdigit():
#            # Create a new state string with 'O' replacing the empty position
#            new_state = state[:i] + move + state[i+1:]
#            next_states.append(new_state)
#    return next_states
#
#player1 = player.QLearnAgent(tag='X', exploration_factor=0)
#player1.load_values()
#
#Q = player1.values
#print(Q)
#
#print("Current state:")
#state = '012345678'
#print_game(state)
#print("Q values for state:")
#print(Q[state])
#def try_na(qt, action):
#    # See if an action exists for a given state, return NA if not
#    # This is needed because in the 'q_values' list comprehension I simply
#    # check all possible board moves.
#    try:
#        return qt[action]
#    except KeyError:
#        return np.nan
#
#
## Possible subsequent states
#next_states = find_next_moves(state, 'O')
#next_Qs = []
#
#for n_state in next_states:
#    try:
#        next_Qs.append(Q[n_state])
#    except KeyError:
#        next_Qs.append(0.0)
#
#for n_Q, n_s in zip(next_Qs, next_states):
#    print("Q-value:", n_Q)
#    print_game(n_s)
